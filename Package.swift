// swift-tools-version:4.0

import PackageDescription

let package = Package(
    name: "GameServer",
    products: [
        .library(
            name: "GameServer",
            targets: ["GameServer"])
    ],
    dependencies: [
        .package(url: "git@gitlab.com:msanford1030/SaaS.git", from: "1.0.1"),
        .package(url: "git@gitlab.com:mancala/gaming-cloud-server-container.git", from: "1.0.4"),
        .package(url: "git@gitlab.com:mancala/gaming-cloud-core.git", from: "1.2.0"),
    ],
    targets: [
        .target(
            name: "GameServer",
            dependencies: ["SaaS", "GamingCloudGameContainer", "GamingCloudCore"])
    ]
)
