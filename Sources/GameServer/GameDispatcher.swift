//
//  GameDispatcher.swift
//  manserver
//
//  Created by Michael Sanford on 12/21/16.
//  Copyright © 2016 flipside5. All rights reserved.
//

import Foundation
import Dispatch
import NetworkingCore
import GamingCore
import SaaS
import GamingCloudGameContainer
import GamingCloudCore

struct UserActvity {
    let numberOfGames: Int
    let numberOfUsers: Int
}

class GameDispatcher {
    
    fileprivate struct IncomingInfo {
        private(set) var joiningPlayers: [GameToken: ClientToken] = [:]
        private(set) var evictingPlayers: [PlayerID] = []
        private(set) var gameMessages: [GameMessage] = []
        private(set) var playerStreams: [PlayerID: StreamData] = [:]
        
        mutating func add(_ gameMessage: GameMessage) {
            gameMessages.append(gameMessage)
        }
        
        mutating func set(_ streamData: StreamData, forPlayer playerID: PlayerID) {
            playerStreams[playerID] = streamData
        }
        
        mutating func addJoinedPlayer(withGameToken gameToken: GameToken, clientToken: ClientToken) {
            joiningPlayers[gameToken] = clientToken
        }
        
        mutating func addEvictedPlayer(withPlayerID playerID: PlayerID) {
            guard !evictingPlayers.contains(playerID) else {
                assert(false, "attemping to evict player that has already been evicted")
                return
            }
            evictingPlayers.append(playerID)
        }
    }
    
    private let domain = "GameDispatcher"
    private unowned var networkService: NetworkService
    private let incomingQueue: DispatchQueue
    private let gameQueue: DispatchQueue
    private var containers: [ContainerID: GameContainer] = [:]
    private var incomingInfo: [ContainerID: IncomingInfo] = [:]
    private var emptyIncomingInfo: [ContainerID: IncomingInfo] = [:]
    private var clientGameTokens: [ClientToken: GameToken] = [:]
    private var nextGameID: GameID = 1
    private var nextBroadcast: [BroadcastStreamInfo] = []
    private let gameUpdateTimer: DispatchSourceTimer
    private let gameTimeLimit: Timestamp32?
    
    init(networkService: NetworkService, gameTimeLimit: Timestamp32? = nil) {
        self.networkService = networkService
        self.gameTimeLimit = gameTimeLimit
        incomingQueue = networkService.callbackQueue
        gameQueue = DispatchQueue.main
        gameUpdateTimer = DispatchSource.makeTimerSource(flags: [], queue: gameQueue)
        
        gameUpdateTimer.setEventHandler {
            self.updateGameStreams()
        }
        // 30 times a second
        gameUpdateTimer.schedule(deadline: .now(), repeating: .milliseconds(33), leeway: .milliseconds(1))
        
        gameQueue.async { [weak self] in
            self?.gameUpdateTimer.resume()
        }
    }
    
    // MARK: on incoming queue
    
    func currentUserActivity() -> UserActvity {
        var numberOfGames: Int = 0
        var numberOfUsers: Int = 0
        
        gameQueue.sync { [weak self] in
            guard let strongSelf = self else { return }
            numberOfGames = strongSelf.containers.count
            numberOfUsers = strongSelf.clientGameTokens.count
        }
        
        return UserActvity(
            numberOfGames: numberOfGames,
            numberOfUsers: numberOfUsers
        )
    }
    
    func popNextGameID() -> GameID {
        let gameID = nextGameID
        nextGameID += 1
        if nextGameID > Int(UInt16.max) {
            nextGameID = 0
        }
        return gameID
    }
    
    fileprivate func createContainer(numberOfPlayers: Int, gameStartupData: Data) -> (ContainerID, GameContainer)? {
        let gameID = popNextGameID()
        let watermark = Watermark(Date.now32)
        
        guard let container = GameContainer(
            networkService: networkService,
            gameID: gameID,
            watermark: watermark,
            gameStartupData: gameStartupData,
            timeLimit: gameTimeLimit) else { return nil }
        
        let containerID = ContainerID(gameID: gameID, watermark: watermark)
        return (containerID, container)
    }
    
    func createGames(with request: CreateGamesRequest) -> CreateGamesResponse {
        let results: ([CreateGameResult], [ContainerID: GameContainer]) = request.createGameInfos.reduce(([], [:])) { result, gameInfo in
            guard let entry = createContainer(numberOfPlayers: gameInfo.numberOfPlayers, gameStartupData: gameInfo.gameStartupInfo) else {
                return (result.0 + [.failure("failure")], result.1)
            }
            var updatedContainers = result.1
            updatedContainers[entry.0] = entry.1
            
            var updatedResults = result.0
            updatedResults.append(.success(entry.1.gameClientInfo))
            return (updatedResults, updatedContainers)
        }
        
        gameQueue.sync { [weak self] in
            results.1.forEach { [weak self] in
                guard let strongSelf = self else { return }
                strongSelf.containers[$0.key] = $0.value
                strongSelf.incomingInfo[$0.key] = IncomingInfo()
                strongSelf.emptyIncomingInfo[$0.key] = IncomingInfo()
            }
        }
        
        return CreateGamesResponse(requestID: request.requestID, createGameResults: results.0)
    }
    
    func registerPlayer(withGameToken gameToken: GameToken, clientToken: ClientToken) {
        clientGameTokens[clientToken] = gameToken
    }
    
    private func evictPlayer(from container: GameContainer, playerID: PlayerID, clientToken: ClientToken) {
        container.playerLeftGame(withPlayerID: playerID)
        clientGameTokens[clientToken] = nil
    }
    
    func evictPlayer(withClientToken clientToken: ClientToken) {
        guard let gameToken = clientGameTokens[clientToken], let container = self.container(forGameToken: gameToken) else {
            return
        }
        evictPlayer(from: container, playerID: gameToken.playerID, clientToken: clientToken)
    }
    
    func evictPlayer(withClientToken clientToken: ClientToken, gameToken: GameToken) {
        guard gameToken == clientGameTokens[clientToken], let container = self.container(forGameToken: gameToken) else {
            log(.warning, domain, "attempt to evict player from game (with game token) that does not exist")
            return
        }
        evictPlayer(from: container, playerID: gameToken.playerID, clientToken: clientToken)
    }
    
    func removePlayer(withClientToken clientToken: ClientToken) {
        clientGameTokens[clientToken] = nil
    }
    
    private func container(forGameToken gameToken: GameToken) -> GameContainer? {
        let id = ContainerID(gameID: gameToken.gameID, watermark: gameToken.watermark)
        return containers[id]
    }
    
    private func incomingInfo(for gameToken: GameToken) -> IncomingInfo? {
        let id = ContainerID(gameID: gameToken.gameID, watermark: gameToken.watermark)
        return incomingInfo[id]
    }
    
    func dispatch(_ joinGameRequest: PlayerJoinGameRequest, clientToken: ClientToken) {
        let containerID = ContainerID(gameToken: joinGameRequest.gameToken)
        guard incomingInfo[containerID] != nil else {
            log(.warning, domain, "received joinGameRequest to game that does not exist")
            return
        }
        clientGameTokens[clientToken] = joinGameRequest.gameToken
        incomingInfo[containerID]?.addJoinedPlayer(withGameToken: joinGameRequest.gameToken, clientToken: clientToken)
    }
    
    func dispatch(_ gameMessage: GameMessage) {
        let containerID = ContainerID(gameToken: gameMessage.gameToken)
        guard incomingInfo[containerID] != nil else {
            log(.warning, domain, "received message to game that does not exist")
            return
        }
        incomingInfo[containerID]?.add(gameMessage)
    }
    
    func dispatch(_ streamData: StreamData, for gameToken: GameToken) {
        
        let containerID = ContainerID(gameToken: gameToken)
        guard incomingInfo[containerID] != nil else {
            log(.warning, domain, "received stream to game that does not exist")
            return
        }
        incomingInfo[containerID]?.set(streamData, forPlayer: gameToken.playerID)
    }
    
    var broadcastStreamInfos: [BroadcastStreamInfo] {
        var result: [BroadcastStreamInfo] = []
        gameQueue.sync { [weak self] in
            guard let strongSelf = self else { return }
            result = strongSelf.nextBroadcast
        }
        return result
    }
    
    // MARK: on game queue
    func updateGameStreams() {
        var incomingInfoCopy: [ContainerID: IncomingInfo] = [:]
        var containersCopy: [ContainerID: GameContainer] = [:]
        
        incomingQueue.sync { [weak self] in
            guard let strongSelf = self else { return }
            incomingInfoCopy = strongSelf.incomingInfo
            strongSelf.incomingInfo = strongSelf.emptyIncomingInfo
            
            let hasInactiveGames = strongSelf.containers.contains { !$0.value.isActive }
            if hasInactiveGames {
                for entry in strongSelf.containers {
                    if entry.value.isActive {
                        containersCopy[entry.key] = entry.value
                    } else {
                        for clientToken in entry.value.clientTokens {
                            strongSelf.removePlayer(withClientToken: clientToken)
                        }
                    }
                }
                
                strongSelf.containers = containersCopy
            } else {
                containersCopy = strongSelf.containers
            }
        }
        
        nextBroadcast = containersCopy.reduce([]) { (result, entry: (containerID: ContainerID, container: GameContainer)) -> [BroadcastStreamInfo] in
            guard let gameIncomingInfo = incomingInfoCopy[entry.containerID] else {
                log(.warning, domain, "internal error: incoming state not found")
                return result
            }
            guard entry.container.isActive else { return result }
            
            for joiningPlayersEntry in gameIncomingInfo.joiningPlayers {
                let gameToken = joiningPlayersEntry.key
                let type: ConnectedPlayerType = .human(gameToken, joiningPlayersEntry.value)
                entry.container.playerJoinedGame(withPlayerID: gameToken.playerID, playerType: type, playerInfoPayload: nil)
            }
            
            let gameStream = entry.container.updateGameState(withPlayerStreams: gameIncomingInfo.playerStreams, gameMessages: gameIncomingInfo.gameMessages)
            return result + gameStream
        }
    }
}

/*-----------------------------------------*/

fileprivate struct ContainerID: Hashable {
    let gameID: GameID
    let watermark: Watermark
    
    var hashValue: Int {
        return watermark
    }
    
    init(gameID: GameID, watermark: Watermark) {
        self.gameID = gameID
        self.watermark = watermark
    }
    
    init(gameToken: GameToken) {
        self.gameID = gameToken.gameID
        self.watermark = gameToken.watermark
    }
}

fileprivate func ==(lhs: ContainerID, rhs: ContainerID) -> Bool {
    return lhs.gameID == rhs.gameID && lhs.watermark == rhs.watermark
}
/*-----------------------------------------*/
/*
 - create/remove a container
 - update all container game states w messages/streams
 - get broadcast stream value
 
 
 */
