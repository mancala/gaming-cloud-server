//
//  GameServer.swift
//
//  Created by Michael Sanford on 12/22/16.
//  Copyright © 2016 flipside5. All rights reserved.
//

import Foundation
import Dispatch
import SaaS
import NetworkingCore
import GamingCore
import GamingCloudCore
import SwiftOnSockets

fileprivate let domain = "GameServer"
fileprivate let isUpdateLobbyEnabled = false

class GameServer {
    let networkService: NetworkService
    let dispatcher: GameDispatcher
    let incomingQueue: DispatchQueue
    let incomingLobbyQueue: DispatchQueue
    private let activityQueue: DispatchQueue
    private var lobbyConnection: ServiceConnection?
    private var lobbyDelegate: LobbyDelegate!
    fileprivate var activityWorkItem: DispatchWorkItem?
    fileprivate var reconnectLobbyWorkItem: DispatchWorkItem?
    
    init(gameTimeLimit: Timestamp32? = nil) throws {
        incomingQueue = DispatchQueue(label: "GameServer.incomingQueue")
        incomingLobbyQueue = DispatchQueue(label: "GameServer.incomingLobbyQueue")
        activityQueue = DispatchQueue(label: "GameServer.activityQueue")
        
        switch GameServerConfig.shared.serverType {
        case .event:
            let service = try EventBasedService(tcpPort: GameServerConfig.shared.portID, callbackQueue: incomingQueue)
            networkService = service
            dispatcher = GameDispatcher(networkService: networkService, gameTimeLimit: gameTimeLimit)
            service.delegate = self
        case .realtime:
            let service = try RealTimeService(udpPort: GameServerConfig.shared.portID, ssrc: 1234, callbackQueue: incomingQueue)
            networkService = service
            dispatcher = GameDispatcher(networkService: networkService, gameTimeLimit: gameTimeLimit)
            service.delegate = self
        }
        lobbyDelegate = LobbyDelegate(parent: self)
    }
    
    func start() throws {
        try networkService.start()
        setupLobbyConnection()
//        connectToLobby()
    }
    
    private func setupLobbyConnection() {
        guard lobbyConnection == nil else { return }
        lobbyConnection = ServiceConnection(type: .tcp, remoteAddress: GameServerConfig.shared.lobbyAddress)
        lobbyConnection?.delegate = lobbyDelegate
        
        do {
            try lobbyConnection?.connect()
        } catch {
            lobbyConnection = nil
            guard reconnectLobbyWorkItem == nil else { return }
            reconnectLobbyWorkItem = DispatchQueue.main.addOperation(1, repeats: true) { [weak self] in
                guard let strongSelf = self else { return }
                strongSelf.setupLobbyConnection()
            }
        }
    }
    
//    private func connectToLobby() {
//        guard lobbyConnection == nil else { return }
//        lobbyConnection = ServiceConnection(type: .tcp, remoteAddress: GameServerConfig.shared.lobbyAddress)
//        lobbyConnection?.delegate = lobbyDelegate
//
//        do {
//            try lobbyConnection?.connect()
//        } catch {
//            print(".")
//        }
//
//        guard case .some(.disconnected) = lobbyConnection?.connectionStatus else { return }
//
//        guard reconnectLobbyWorkItem == nil else { return }
//        reconnectLobbyWorkItem = DispatchQueue.main.addOperation(1, repeats: true) { [weak self] in
//            guard let strongSelf = self else { return }
//            do {
//                try strongSelf.lobbyConnection?.connect()
//            } catch {
//                print(".")
//            }
//        }
//    }
    
    fileprivate func onDidConnectToLobby() {
        print("connected to lobby")
        
        reconnectLobbyWorkItem?.cancel()
        reconnectLobbyWorkItem = nil
        
        let checkin = GameServerCheckin(connectionAddress: GameServerConfig.shared.externalAddress)
        sendLobbyMessage(type: GameServerToLobbyMessageType.gameServerCheckin.rawValue, payloadInfo: checkin)
        
        activityWorkItem = activityQueue.addOperation(10, repeats: true) { [weak self] in
            self?.sendActivityUpdateToLobby()
        }
    }
    
    fileprivate func onDidDisconnectToLobby() {
        activityWorkItem?.cancel()
        activityWorkItem = nil
        
        DispatchQueue.main.asyncAfter(timeInterval: 1) { [weak self] in
            self?.lobbyConnection = nil
            self?.setupLobbyConnection()
        }
    }
    
    fileprivate func sendLobbyMessage(type: MessageType, payloadInfo: ByteCoding) {
        guard let connection = lobbyConnection else { return }
        DispatchQueue.main.async {
            do {
                let message = Message(type: type, payloadInfo: payloadInfo)
                try connection.send(message)
            } catch {
                log(.warning, domain, "failed to send message to lobby; error=\(error)")
            }
        }
    }
    
    fileprivate func sendActivityUpdateToLobby() {
        guard isUpdateLobbyEnabled else {
            let activity = dispatcher.currentUserActivity()
            log(.warning, domain, "ACTIVITY: games=\(activity.numberOfGames), users=\(activity.numberOfUsers)")
            return
        }
        
        let activity = dispatcher.currentUserActivity()
        let payload = GameServerStatusUpdate(numberOfGames: activity.numberOfGames, numberOfUsers: activity.numberOfUsers)
        sendLobbyMessage(type: GameServerToLobbyMessageType.gameServerStatusUpdate.rawValue, payloadInfo: payload)
    }
}

/*-----------------------------------------*/

extension GameServer: RealTimeServiceDelegate {
    func service(_ service: NetworkService, didConnectToClient token: ClientToken) {
        do {
            try service.sendPing(toClient: token) {
                log(.debug, domain, "ping to client = \($0)")
            }
        } catch {
        }
    }
    
    func service(_ service: NetworkService, didDisconnectFromClient token: ClientToken, reason: DisconnectReason) {
        log(.debug, "GameServer", "client did disconnect")
        dispatcher.evictPlayer(withClientToken: token)
    }
    
    func service(_ service: NetworkService, didReceiveMessage message: Message, fromClient token: ClientToken) {
        guard let type = ClientToGameServerMessageType(rawValue: message.type) else {
            assert(false, "received message of unknown type (\(message.type))")
            return
        }
        
        switch type {
        case .gameToken:
            guard let payload = message.payload else {
                assert(false, "received game message without any data")
                return
            }
            
            let unarchiver = ByteUnarchiver(archive: payload)
            guard let gameMessage = GameMessage(unarchiver: unarchiver) else {
                assert(false, "received game message but unarchived failed")
                return
            }
            
            dispatcher.dispatch(gameMessage)
            
        case .joinGame:
            guard let payload = message.payload else {
                assert(false, "received game message without any data")
                return
            }
            
            let unarchiver = ByteUnarchiver(archive: payload)
            guard let joinGameRequest = PlayerJoinGameRequest(unarchiver: unarchiver) else {
                assert(false, "received PlayerJoinGameRequest but unarchived failed")
                return
            }
            
            dispatcher.dispatch(joinGameRequest, clientToken: token)
                        
        default:
            print("!!! unexpected message = \(type)")
        }
    }
    
    func service(_ service: RealTimeService, didReceiveStream gameStreamData: StreamData, fromClient token: ClientToken) {
        let unarchiver = ByteUnarchiver(archive: gameStreamData.payload)
        guard let gameToken = GameToken(unarchiver: unarchiver) else {
            assert(false, "received game stream data but unarchived failed")
            return
        }
        
        let streamData = StreamData(version: gameStreamData.version, payload: unarchiver.remainingArchive)
        dispatcher.dispatch(streamData, for: gameToken)
    }
    
    func serviceBroadcastStreamInfo(_ service: RealTimeService) -> [BroadcastStreamInfo] {
        return dispatcher.broadcastStreamInfos
    }
}

/*-----------------------*/

class LobbyDelegate: ServiceConnectionDelegate {
    
    private unowned let parent: GameServer
    
    init(parent: GameServer) {
        self.parent = parent
    }
    
    func connection(didConnect connection: ServiceConnection) {
        DispatchQueue.main.async { [weak self] in
            self?.parent.onDidConnectToLobby()
        }
    }
    
    func connection(didDisconnect connection: ServiceConnection, error: Error?) {
        DispatchQueue.main.async { [weak self] in
            self?.parent.onDidDisconnectToLobby()
        }
    }
    
    func connection(_ connection: ServiceConnection, didReceiveMessage message: Message) {
        parent.incomingLobbyQueue.async { [unowned self] in
            guard let type = LobbyToGameServerMessageType(rawValue: message.type) else {
                assert(false, "received lobby message of unknown type (\(message.type))")
                return
            }
            switch type {
            case .createGamesRequest:
                guard let payload = message.payload else {
                    assert(false, "payloading missing from CreateGamesRequest")
                    return
                }
                let unarchiver = ByteUnarchiver(archive: payload)
                guard let request = CreateGamesRequest(unarchiver: unarchiver) else {
                    assert(false, "failed to unarchive CreateGamesRequest")
                    return
                }
                let response = self.parent.dispatcher.createGames(with: request)
                self.parent.sendLobbyMessage(type: response.type.rawValue, payloadInfo: response)
            }
        }
    }
    
    func connection(_ connection: ServiceConnection, didReceiveStream: StreamData) {}
}

/*-----------------------*/

enum URLSessionError: Error {
    case network(Error)
    case unknown
}

extension URLSession {
    func sendSynchronousStringRequest(_ request: URLRequest) throws -> (URLResponse, String) {
        var result: (URLResponse, String)? = nil
        var error: Error? = nil
        let semaphore = DispatchSemaphore(value: 0)
        let task = self.dataTask(with: request) { data, response, requestError in
            guard let response = response, let data = data else {
                error = requestError
                return
            }
            guard let string = String(data: data, encoding: .utf8) else {
                error = URLSessionError.unknown
                return
            }
            result = (response, string)
            semaphore.signal()
        }
        
        task.resume()
        _ = semaphore.wait(timeout: .distantFuture)
        
        guard let requestResult = result else {
            guard let error = error else { throw URLSessionError.unknown }
            throw URLSessionError.network(error)
        }
        return requestResult
    }
}

/*-----------------------*/

public class GameServerTimer: GameTimerProtocol {
    
    public let timeInterval: TimeInterval
    private let repeats: Bool
    private let tolerance: TimeInterval
    private let handler: () -> ()
    public private(set) var fireDate: Date
    public private(set) var isValid: Bool = true
    
    init(timeInterval: TimeInterval, repeats: Bool, tolerance: TimeInterval = 0.01, handler: @escaping () -> ()) {
        self.timeInterval = timeInterval
        self.repeats = repeats
        self.tolerance = tolerance
        self.handler = handler
        fireDate = Date().addingTimeInterval(timeInterval)
    }
    
    public func invalidate() {
        isValid = false
    }
    
    public func fireIfNeeded() {
        guard fireDate.timeIntervalSinceNow < tolerance else { return }
        fire()
    }
    
    func fire() {
        guard isValid else { return }
        if repeats {
            fireDate = Date().addingTimeInterval(timeInterval)
        } else {
            isValid = false
        }
        handler()
    }
    
}
