//
//  GameContainer.swift
//  manserver
//
//  Created by Michael Sanford on 12/21/16.
//  Copyright © 2016 flipside5. All rights reserved.
//

import Foundation
import NetworkingCore
import SaaS
import GamingCloudGameContainer
import GamingCore
import GamingCloudCore

fileprivate enum GameSessionState {
    case waitingForPlayersToJoin, playing, gameOver, inactive
    
    var isActive: Bool {
        switch self {
        case .waitingForPlayersToJoin, .playing, .gameOver:
            return true
        case .inactive:
            return false
        }
    }
}

/*-----------------------------------------*/

class GameContainer: ParentGameContainer {
    
    private enum GameContainerValue: RealTimeGameServerContainerProtocol {
        case realtime(RealTimeGameServerContainerProtocol)
        case event(GameServerContainerProtocol)
        
        init(gameService: GameServiceRef, gameStartupData: Data) {
            assert(false, "init sub on GameContainerValue should never be invoked")
            exit(-1)
        }
        
        private var untypeContainer: GameServerContainerProtocol {
            switch self {
            case .realtime(let impl): return impl
            case .event(let impl): return impl
            }
        }
        
        var playerTypes: [PlayerID: PlayerType] {
            return untypeContainer.playerTypes
        }
        
        var gameInfo: Data {
            return untypeContainer.gameInfo
        }
        
        func onAllHumanPlayersJoined() {
            untypeContainer.onAllHumanPlayersJoined()
        }
        
        func onPlayerDidJoin(withPlayerID playerID: PlayerID, playerType: ConnectedPlayerType, playerInfoPayload: Data?) {
            untypeContainer.onPlayerDidJoin(withPlayerID: playerID, playerType: playerType, playerInfoPayload: playerInfoPayload)
        }
        
        func onPlayerDidLeave(withID playerID: PlayerID, remainingHumanPlayerCount: Int, spectatorCount: Int) {
            untypeContainer.onPlayerDidLeave(withID: playerID, remainingHumanPlayerCount: remainingHumanPlayerCount, spectatorCount: spectatorCount)
        }
        
        func onGameStateUpdate(withTime time: Timestamp32, playerStreams: [PlayerID: StreamData]) -> BroadcastStreamData {
            switch self {
            case .realtime(let impl):
                return impl.onGameStateUpdate(withTime: time, playerStreams: playerStreams)
            case .event:
                assert(false, "attempt real-time game state update operation on event-based container")
                return .all(data: nil)
            }
        }
        
        func onGameStateUpdate(withTime time: Timestamp32, messages: [PlayerMessage]) {
            untypeContainer.onGameStateUpdate(withTime: time, messages: messages)
        }
        
        static func create(withGameService gameService: GameServiceRef, gameStartupData: Data) -> GameContainerValue? {
            switch GameServerConfig.shared.serverType {
            case .realtime(let containerType):
                guard let instance = containerType.init(gameService: gameService, gameStartupData: gameStartupData) else { return nil }
                return .realtime(instance)
            case .event(let containerType):
                guard let instance = containerType.init(gameService: gameService, gameStartupData: gameStartupData) else { return nil }
                return .event(instance)
            }
        }
    }
    
    private let domain = "GameContainer"
    private let networkService: NetworkService
    let gameID: GameID
    let watermark: Watermark
//    var playerInfos: [GamePlayerInfo]
    private var spectators: [ClientToken]
    private var playerStates: [PlayerSessionState]
    private var gameSessionState: GameSessionState
    private var clock: Clock
    private var timeLimit: Timestamp32?
    
    private var impl: GameContainerValue!
    
    init?(networkService: NetworkService, gameID: GameID, watermark: Watermark, gameStartupData: Data, timeLimit: Timestamp32? = nil) {
        self.networkService = networkService
        self.gameID = gameID
        self.watermark = watermark
        self.timeLimit = timeLimit
        //self.playerInfos = playerInfos
        clock = Clock(startImmediately: true)
        spectators = []
        gameSessionState = .waitingForPlayersToJoin
        playerStates = []
        
        let ref = GameServiceRef(container: self)
        guard let impl = GameContainerValue.create(withGameService: ref, gameStartupData: gameStartupData) else { return nil }
        self.impl = impl
        
        playerStates = impl.playerTypes.map {
            switch $0.value {
            case .human:
                let gameToken = GameToken(gameID: gameID, watermark: watermark, playerID: $0.key)
                return .waitingToJoin(.human(gameToken))
            case .bot:
                return .joined(.bot)
            }
        }
        startGameIfNeeded()
    }
    
    var gameClientInfo: GameClientInfo {
        return GameClientInfo(gameTokens: gameTokens, gameInfo: impl.gameInfo)
    }
    
    var gameTokens: [PlayerID: GameToken] {
        return playerStates.enumerated().reduce([:]) { result, entry in
            guard let gameToken = entry.element.gameToken else { return result }
            var updatedResult = result
            updatedResult[entry.offset] = gameToken
            return updatedResult
        }
    }
    
    var clientTokens: [ClientToken] {
        return playerStates.compactMap { $0.clientToken } + spectators
    }
    
    var isActive: Bool {
        guard let timeLimit = timeLimit else { return gameSessionState.isActive }
        return gameSessionState.isActive && clock.now < timeLimit
    }
    
    private func startGameIfNeeded() {
        let hasEveryoneJoined = playerStates.reduce(true) { (result, state) in
            guard result else { return false }
            switch state {
            case .waitingToJoin, .left: return false
            case .joined: return true
            }
        }
        
        if hasEveryoneJoined {
            gameSessionState = .playing
            impl.onAllHumanPlayersJoined()
        }
    }
    
    // MARK: Service --> Container
    
    private var humanClientTokens: [ClientToken] {
        return playerStates.compactMap {
            guard case .joined(let playerType) = $0, case .human(_, let clientToken) = playerType else { return nil }
            return clientToken
        }
    }
    
    private var humanPlayerIDsAndClientTokens: [(PlayerID, ClientToken)] {
        return playerStates.enumerated().compactMap {
            guard case .joined(let playerType) = $0.element, case .human(_, let clientToken) = playerType else { return nil }
            return ($0.offset, clientToken)
        }
    }
    
    private var humanPlayerIDsAndTokens: [(PlayerID, GameToken, ClientToken)] {
        return playerStates.enumerated().compactMap {
            guard case .joined(let playerType) = $0.element, case .human(let gameToken, let clientToken) = playerType else { return nil }
            return ($0.offset, gameToken, clientToken)
        }
    }
    
    func updateGameState(withPlayerStreams playerStreams: [PlayerID: StreamData], gameMessages: [GameMessage]) -> [BroadcastStreamInfo] {
        let incomingPlayerMessages = gameMessages.map { PlayerMessage(playerID: $0.gameToken.playerID, message: $0.message) }
        
        // fire timers
        if !timers.isEmpty {
            for timer in timers {
                timer.fireIfNeeded()
            }
            timers = timers.filter { $0.isValid }
        }
        
        if !incomingPlayerMessages.isEmpty {
            impl.onGameStateUpdate(withTime: clock.now, messages: incomingPlayerMessages)
        }
        
        let streamDataResult = impl.onGameStateUpdate(withTime: clock.now, playerStreams: playerStreams)
        
        switch streamDataResult {
        case .all(let streamData):
            return [BroadcastStreamInfo(streamData: streamData, clientTokens: humanJoinedPlayerClientTokens + spectators)]
        case .specialized(let playerData, let spectatorData):
            let playerStreamInfo = BroadcastStreamInfo(streamData: playerData, clientTokens: humanJoinedPlayerClientTokens)
            let spectatorStreamInfo = BroadcastStreamInfo(streamData: spectatorData, clientTokens: spectators)
            return [playerStreamInfo, spectatorStreamInfo]
        case .playerSpecialized(let playerData, let spectatorData):
            let playerClientTokens = self.humanPlayerIDsAndClientTokens
            guard playerData.count == playerClientTokens.count else {
                log(.warning, domain, "updateGameState: missing value. just taking a guess as to what to do here.")
                let playerStreamInfo = BroadcastStreamInfo(streamData: nil, clientTokens: humanJoinedPlayerClientTokens)
                let spectatorStreamInfo = BroadcastStreamInfo(streamData: spectatorData, clientTokens: spectators)
                return [playerStreamInfo, spectatorStreamInfo]
            }
            
            let playerStreamInfos = humanPlayerIDsAndClientTokens.map { valuePair -> BroadcastStreamInfo in
                let (playerID, clientToken) = valuePair
                guard let playerStreamData = playerData[playerID] else {
                    log(.warning, self.domain, "updateGameState: missing value for player (playerID=\(playerID)")
                    return BroadcastStreamInfo(streamData: nil, clientTokens: [clientToken])
                }
                return BroadcastStreamInfo(streamData: playerStreamData, clientTokens: [clientToken])
            }
            let spectatorStreamInfo = BroadcastStreamInfo(streamData: spectatorData, clientTokens: spectators)
            return playerStreamInfos + [spectatorStreamInfo]
        }
    }
    
    func playerJoinedGame(withPlayerID playerID: PlayerID, playerType: ConnectedPlayerType, playerInfoPayload: Data?) {
        playerStates[playerID] = .joined(playerType)
        
        switch gameSessionState {
        case .waitingForPlayersToJoin:
            impl.onPlayerDidJoin(withPlayerID: playerID, playerType: playerType, playerInfoPayload: playerInfoPayload)
            startGameIfNeeded()
            
        default: ()
        }
    }
    
    func playerLeftGame(withPlayerID playerID: PlayerID) {
        playerStates[playerID] = .left
        
        let humanPlayerCount: Int = playerStates.reduce(0) { (result, state) in state.playerType?.isHuman == true ? result + 1 : result }
        impl.onPlayerDidLeave(withID: playerID, remainingHumanPlayerCount: humanPlayerCount, spectatorCount: spectators.count)
        
        if humanPlayerCount == 0 {
            gameSessionState = .inactive
        }
    }
    
    func spectatorJoinedGame(withToken token: ClientToken) {
        guard spectators.index(of: token) == nil else {
            assert(false, "attempt to add spectator which is already observing")
            return
        }
        spectators.append(token)
    }
    
    func spectatorLeftGame(withToken token: ClientToken) {
        guard let index = spectators.index(of: token) else {
            assert(false, "attempt to remove spectator which isn't an observing")
            return
        }
        spectators.remove(at: index)
    }
    
    private var humanJoinedPlayerClientTokens: [ClientToken] {
        return playerStates.compactMap {
            guard case .joined(let playerType) = $0, case .human(_, let clientToken) = playerType else { return nil }
            return clientToken
        }
    }
    
    func broadcast(messages: BroadcastMessages, completion: (() -> ())?) {
        switch messages {
        case .all(let messages):
            let tokens = humanJoinedPlayerClientTokens + spectators
            networkService.broadcast(messages: messages, toClients: tokens, completion: completion)
            
        case .specialized(let playerMessages, let spectatorMessages):
            if let playerMessages = playerMessages {
                networkService.broadcast(messages: playerMessages, toClients: humanJoinedPlayerClientTokens, completion: completion)
            }
            
            if let spectatorMessages = spectatorMessages {
                networkService.broadcast(messages: spectatorMessages, toClients: spectators, completion: completion)
            }
            
        case .playerSpecialized(let playerMessages, let spectatorMessages):
            playerMessages.forEach { [weak self] keyValuePair in
                guard let strongSelf = self else { return }
                let (playerID, messages) = keyValuePair
                guard case .joined(let playerType) = strongSelf.playerStates[playerID], case .human(_, let clientToken) = playerType else { return }
                strongSelf.networkService.broadcast(messages: messages, toClients: [clientToken], completion: completion)
            }
            
            if let spectatorMessages = spectatorMessages {
                networkService.broadcast(messages: spectatorMessages, toClients: spectators, completion: completion)
            }
        }
    }
    
    func broadcast(messages: BroadcastMessages) {
        broadcast(messages: messages, completion: nil)
    }
    
    // MARK: Container --> Service
    
    var gameTokenForSpectator: GameToken {
        return GameToken(gameID: gameID, watermark: watermark, playerID: PlayerID.spectator)
    }
    
    func playerSessionState(forPlayerID playerID: PlayerID) -> PlayerSessionState {
        return playerStates[playerID]
    }
    
    func send(message: Message, toPlayer playerID: PlayerID) {
        guard case .joined(let playerType) = playerStates[playerID], case .human(_, let clientToken) = playerType else {
            assert(false, "attempt to send remote message to a bot or disconnected human")
            return
        }
        
        do {
            try networkService.send(message: message, toClient: clientToken)
        } catch {
            log(.warning, domain, "failed to send message (error=\(error))")
        }
    }
    
    func sendSpectators(message: Message) {
        networkService.broadcast(message: message, toClients: spectators)
    }
    
    func endGameSession() {
        endGameSession(withReason: .sessionEnded)
    }
    
    func endGameSession(withReason reason: DisconnectReason) {
        guard gameSessionState.isActive else { return }
        gameSessionState = .inactive
        
        playerStates = Array(repeatElement(.left, count: playerStates.count))
        timers = []
        
        do {
            let allConnectedTokens = spectators + humanJoinedPlayerClientTokens
            for token in allConnectedTokens {
                try networkService.disconnect(fromClient: token, reason: reason)
            }
        } catch (let error) {
            assert(false, "disconnecting had an unexpected error (\(error))")
        }
    }
    
    private(set) var timers: [GameTimerProtocol] = []
    
    func scheduleTimer(withTimeInterval timeInterval: TimeInterval, repeats: Bool, callback: @escaping () -> ()) -> GameTimerProtocol {
        let timer = GameServerTimer(timeInterval: timeInterval, repeats: repeats, handler: callback)
        timers.append(timer)
        return timer
    }
}
