//
//  mainWrapper.swift
//  gserver
//
//  Created by Michael Sanford on 4/12/17.
//
//

import Foundation
import GamingCloudGameContainer
import NetworkingCore

public func runGameServer(with serverType: GameServerConfig.ServerType? = nil, gameTimeLimit: Timestamp32? = nil) {
    print("Game Server starting")
    
    logLevel = .warning
    
    do {
        try GameServerConfig.parseAndShare(fromArgs: CommandLine.arguments, serverType: serverType)
        let server = try GameServer(gameTimeLimit: gameTimeLimit)
        try server.start()
        print("Game Server running")
        log(.debug, "mancala-game-server", "game server is running")
        RunLoop.current.run()
    } catch (let error) {
        print("error=\(error)")
    }
    
    print("Game Server shutdown")
}
