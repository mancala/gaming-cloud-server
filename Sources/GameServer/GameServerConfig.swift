//
//  GameServerConfig.swift
//
//  Created by Michael Sanford on 1/7/17.
//  Copyright © 2017 flipside5. All rights reserved.
//

import Foundation
import GamingCore
import SwiftOnSockets
import GamingCloudGameContainer

enum ParseError: Error, CustomStringConvertible {
    case MissingGameServerPortArg
    case MissingGameServerPortValue
    case InvalidGameServerPortValue
    case MissingLobbyAddressArg
    case MissingLobbyAddressValue
    case InvalidLobbyAddressValue
    case MissingExternalAddressArg
    case MissingExternalAddressValue
    case InvalidExternalAddressValue
    case MissingGameServerTypeArg
    case MissingGameServerTypeValue
    case InvalidGameServerTypeValue
    case MissingGameContainerArg
    case MissingGameContainerClassName
    case InvalidGameContainerClassName(String)
    case NonConformingGameContainerClass(String)
    
    var description: String {
        switch self {
        case .MissingGameServerPortArg: return "missing required game server port arg (-port)"
        case .MissingGameServerPortValue: return "missing required game server port value (i.e. 8088)"
        case .InvalidGameServerPortValue: return "invalid game server port (i.e. 8088)"
        case .MissingLobbyAddressArg: return "missing required lobby address arg (-lobby)"
        case .MissingLobbyAddressValue: return "missing required lobby address value (i.e. 192.1.1.5:8080)"
        case .InvalidLobbyAddressValue: return "invalid lobby address value (i.e. 192.1.1.5:8080"
        case .MissingExternalAddressArg: return "missing required external address arg (-external)"
        case .MissingExternalAddressValue: return "missing required external address value (i.e. 208.8.8.92:8080)"
        case .InvalidExternalAddressValue: return "invalid required external address value (i.e. 208.8.8.92:8080)"
        case .MissingGameServerTypeArg: return "missing required game server type arg (-type)"
        case .MissingGameServerTypeValue: return "missing required game server value (rt | eb)"
        case .InvalidGameServerTypeValue: return "invalid game server value (rt | eb)"
        case .MissingGameContainerArg: return "missing required game container arg (-gc)"
        case .MissingGameContainerClassName: return "missing game container class name. required after '-gc'"
        case .InvalidGameContainerClassName(let name): return "invalid game container class name (\(name))"
        case .NonConformingGameContainerClass(let name): return "game container class does not conform to protocol \(name)"
        }
    }
}

public class GameServerConfig {
    
    public enum ServerType {
        case realtime(RealTimeGameServerContainerProtocol.Type)
        case event(GameServerContainerProtocol.Type)
    }
    
    static private(set) var shared: GameServerConfig!
    
    let serverType: ServerType
    let portID: PortID
    let lobbyAddress: IPAddress
    let externalAddress: IPAddress
    
    init(serverType: ServerType, portID: PortID, lobbyAddress: IPAddress, externalAddress: IPAddress) {
        self.serverType = serverType
        self.portID = portID
        self.lobbyAddress = lobbyAddress
        self.externalAddress = externalAddress
    }
    
    static func parseAndShare(fromArgs args: [String], serverType candidateServerType: ServerType? = nil) throws {
        // Parse for Port
        guard let portIndex = args.index(of: "-port") else { throw ParseError.MissingGameServerPortArg }
        let portValueIndex = portIndex + 1
        guard portValueIndex < args.count else { throw ParseError.MissingGameServerPortValue }
        let portValue = args[portValueIndex]
        guard let portID = PortID(portValue) else { throw ParseError.InvalidGameServerPortValue }
        
        // Parse for Lobby ipAddress/port
        guard let lobbyIndex = args.index(of: "-lobby") else { throw ParseError.MissingLobbyAddressArg }
        let lobbyValueIndex = lobbyIndex + 1
        guard lobbyValueIndex < args.count else { throw ParseError.MissingLobbyAddressValue }
        let lobbyValue = args[lobbyValueIndex]
        guard let lobbyAddress = IPAddress(formatted: lobbyValue) else { throw ParseError.InvalidLobbyAddressValue }
        
        // Parse for optional connectvity ipAddress/port
        guard let externalAddressIndex = args.index(of: "-external") else { throw ParseError.MissingExternalAddressArg }
        let externalAddressValueIndex = externalAddressIndex + 1
        guard externalAddressIndex < args.count else { throw ParseError.MissingExternalAddressValue }
        let externalAddressValue = args[externalAddressValueIndex]
        guard let externalAddress = IPAddress(formatted: externalAddressValue) else { throw ParseError.InvalidExternalAddressValue }
        
        // Parse for type
        let serverType: ServerType
        if let candidateServerType = candidateServerType {
            serverType = candidateServerType
        } else {
            guard let typeIndex = args.index(of: "-type") else { throw ParseError.MissingGameServerTypeArg }
            let typeValueIndex = typeIndex + 1
            guard typeValueIndex < args.count else { throw ParseError.MissingGameServerTypeValue }
            let typeValue = args[typeValueIndex]
            
            guard let gcIndex = args.index(of: "-gc") else { throw ParseError.MissingGameContainerArg }
            let gcClassNameIndex = gcIndex + 1
            guard gcClassNameIndex < args.count else { throw ParseError.MissingGameContainerClassName }
            let gcClassName = args[gcClassNameIndex]
            guard let someClass = NSClassFromString(gcClassName) else { throw ParseError.InvalidGameContainerClassName(gcClassName) }
            
            switch typeValue {
            case "rt":
                guard let containerType = someClass.self as? RealTimeGameServerContainerProtocol.Type else { throw ParseError.NonConformingGameContainerClass(gcClassName) }
                serverType = .realtime(containerType)
                
            case "eb":
                guard let containerType = someClass.self as? GameServerContainerProtocol.Type else { throw ParseError.NonConformingGameContainerClass(gcClassName) }
                serverType = .event(containerType)
                
            default:
                throw ParseError.InvalidGameServerTypeValue
            }
        }
        
        GameServerConfig.shared = GameServerConfig(serverType: serverType, portID: portID, lobbyAddress: lobbyAddress, externalAddress: externalAddress)
    }
}
